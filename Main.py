import numpy as np
import time
from pynput.keyboard import Key, Controller

from PIL import ImageGrab
import cv2

keyboard = Controller()

blueArrow = {"low": (107, 186, 142), "high": (123, 255, 255)}
greenArrow = {"low": (65, 144, 135), "high": (83, 207, 244)}
orangeArrow = {"low": (10, 190, 183), "high": (22, 248, 218)}
yellowArrow = {"low": (21, 248, 255), "high": (39, 253, 255)}

arrowsValue = [blueArrow, greenArrow, yellowArrow, orangeArrow]
keys = [Key.right, Key.down, Key.up, Key.left]



def get_final_contour(thresh_values, hsv):

    threshold = cv2.inRange(hsv, thresh_values["low"], thresh_values["high"])

    kernel = np.ones((5, 5), np.uint8)

    opening = cv2.morphologyEx(threshold, cv2.MORPH_OPEN, kernel)

    closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel)

    contours, hierarchy = cv2.findContours(closing, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    contours = sorted(contours, key=lambda x: int(cv2.moments(x)['m01'] / cv2.moments(x)['m00']))

    result = []

    for cnt1 in contours:
        if cv2.contourArea(cnt1) > 3000:
            result.append(cnt1)

    if len(result) > 0:
        return result[0]
    else:
        return None

while True:

    image = np.array(ImageGrab.grab())
    hsv = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)


    arrows = []
    for x in arrowsValue:
        cnt = get_final_contour(x, hsv)
        arrows.append(cnt)

    for i in range(0, 4):
        cnt = arrows[i]
        if cnt is None:
            continue
        M = cv2.moments(cnt)
        cy = int(M['m01'] / M['m00'])
        cx = int(M['m10'] / M['m00'])

        if cy <= 430:
            keyboard.press(keys[i])
            keyboard.release(keys[i])
            time.sleep(0.04)

    k = cv2.waitKey(33) & 0xFF